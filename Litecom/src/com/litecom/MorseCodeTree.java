package com.litecom;

public class MorseCodeTree {		
		
		private BinaryNode<String> _Root ;
		private static MorseCodeTree _DecoderInstance;
		
		static public MorseCodeTree GetDecoderInstance()
		{
			if (null == _DecoderInstance)
				_DecoderInstance = new MorseCodeTree();
			return _DecoderInstance;
		}
		
		// Dot is left
		// Dash is right
		// Reference of graph from http://en.wikipedia.org/wiki/Morse_code
		private MorseCodeTree()
		{		
			// The graph is built from bottom to the top
			// The root node is at the end
			
			BinaryNode<String> _5_Node = new BinaryNode<String>("5");						
			BinaryNode<String> _4_Node = new BinaryNode<String>("4");
				BinaryNode<String> H_Node = new BinaryNode<String>("H", _5_Node, _4_Node);
							
			BinaryNode<String> _S_Node = new BinaryNode<String>("Š");						
			BinaryNode<String> _3_Node = new BinaryNode<String>("3");
				BinaryNode<String> V_Node = new BinaryNode<String>("V",_S_Node,_3_Node);
					BinaryNode<String> S_Node = new BinaryNode<String>("S", H_Node, V_Node);				
		
				BinaryNode<String> _E_Node = new BinaryNode<String>("É");
					BinaryNode<String> F_Node = new BinaryNode<String>("F",_E_Node,null);

			BinaryNode<String> QuestionMark_Node = new BinaryNode<String>("?");
			BinaryNode<String> Underline_Node = new BinaryNode<String>("_");
				BinaryNode<String> _D_Node = new BinaryNode<String>("Ð", QuestionMark_Node, Underline_Node);
				BinaryNode<String> _2_Node = new BinaryNode<String>("2");
					BinaryNode<String> _U_Node = new BinaryNode<String>("Ü", _D_Node, _2_Node);
						BinaryNode<String> U_Node = new BinaryNode<String>("U", F_Node,_U_Node);
							
							BinaryNode<String> I_Node = new BinaryNode<String>("I",S_Node,U_Node);
		

			BinaryNode<String> Quotation_Node = new BinaryNode<String>("\"");
				BinaryNode<String> __E_Node = new BinaryNode<String>("È",Quotation_Node, null);
					BinaryNode<String> L_Node = new BinaryNode<String>("L", null , __E_Node);
		BinaryNode<String> Dot_Node = new BinaryNode<String>(".");
			BinaryNode<String> Plus_Node = new BinaryNode<String>("+", null, Dot_Node);
				BinaryNode<String> _A_Node = new BinaryNode<String>("Ä",Plus_Node , null );
					BinaryNode<String> R_Node = new BinaryNode<String>("R", L_Node, _A_Node);
					
			BinaryNode<String> Thorn_Node = new BinaryNode<String>("Þ");					
		BinaryNode<String> AT_Mark_Node = new BinaryNode<String>("@");
			BinaryNode<String> __A_Node = new BinaryNode<String>("À",AT_Mark_Node,null);
				BinaryNode<String> P_Node = new BinaryNode<String>("P", Thorn_Node, __A_Node);


			BinaryNode<String> J_With_Accent_Circonflexe_Node = new BinaryNode<String>("J"); // can not find it in ASCII.WTF
		BinaryNode<String> _Single_Quotation_Node = new BinaryNode<String>("'");
			BinaryNode<String> _1_Node = new BinaryNode<String>("1",_Single_Quotation_Node,null );
				BinaryNode<String> J_Node = new BinaryNode<String>("J", J_With_Accent_Circonflexe_Node, _1_Node);
					BinaryNode<String> W_Node = new BinaryNode<String>("W", P_Node, J_Node);

						BinaryNode<String> A_Node = new BinaryNode<String>("A",R_Node, W_Node);
							
							BinaryNode<String> E_Node = new BinaryNode<String>("E", I_Node, A_Node);											
				
		BinaryNode<String> Dash_Node = new BinaryNode<String>("-");
			BinaryNode<String> _6_Node = new BinaryNode<String>("6", null, Dash_Node);
			BinaryNode<String> Equal_Node = new BinaryNode<String>("=");
				BinaryNode<String> B_Node = new BinaryNode<String>("B",_6_Node, Equal_Node);
				
			BinaryNode<String> Slash_Node = new BinaryNode<String>("/");
				BinaryNode<String> X_Node = new BinaryNode<String>("X",Slash_Node, null);
					BinaryNode<String> D_Node = new BinaryNode<String>("D", B_Node, X_Node);							
				
				BinaryNode<String> CA_Node = new BinaryNode<String>("Ç");
			BinaryNode<String> Semi_Colonm_Node = new BinaryNode<String>(";");
			BinaryNode<String> Exclamation_Mark_Node = new BinaryNode<String>("!");
				BinaryNode<String> Blank1_Node = new BinaryNode<String>(" ",Semi_Colonm_Node,Exclamation_Mark_Node);
					BinaryNode<String> C_Node = new BinaryNode<String>("C",CA_Node,Blank1_Node);
					BinaryNode<String> Y_Node = new BinaryNode<String>("Y");
						BinaryNode<String> K_Node = new BinaryNode<String>("K",C_Node,Y_Node);				
							BinaryNode<String> N_Node = new BinaryNode<String>("N",D_Node,K_Node);
			BinaryNode<String> _7_Node = new BinaryNode<String>("7");
		BinaryNode<String> Comma_Node = new BinaryNode<String>(",");
			BinaryNode<String> Blank2_Node = new BinaryNode<String>(" ",null,Comma_Node);
				BinaryNode<String> Z_Node = new BinaryNode<String>("Z", _7_Node, Blank2_Node);							 
				BinaryNode<String> Q_Node = new BinaryNode<String>("Q");
			// Strange G 
			// Strange N
				BinaryNode<String> G_Node = new BinaryNode<String>("G", Z_Node,Q_Node);									
		
				BinaryNode<String> Colonm_Node = new BinaryNode<String>(":");
					BinaryNode<String> _8_Node = new BinaryNode<String>("8", Colonm_Node, null);
						BinaryNode<String> _O_Node = new BinaryNode<String>("Ö", _8_Node, null);
		
			BinaryNode<String> _9_Node = new BinaryNode<String>("9");
			BinaryNode<String> _0_Node = new BinaryNode<String>("0");
				BinaryNode<String> CH_Node = new BinaryNode<String>("CH",_9_Node, _0_Node);
					BinaryNode<String> O_Node = new BinaryNode<String>("O",_O_Node, CH_Node);
						BinaryNode<String> M_Node = new BinaryNode<String>("M",G_Node, O_Node);
							BinaryNode<String> T_Node = new BinaryNode<String>("T",N_Node, M_Node);
								_Root = new BinaryNode<String>("ROOT", E_Node,T_Node);
		}
		 
		public String Decode(String iCodeString) throws NullPointerException
		{
			StringBuilder Result = new StringBuilder() ;
			BinaryNode<String> Node = _Root;
			int Size = iCodeString.length();
			for(int ii = 0 ; ii < Size ; ii++ )
			{
				char Code = iCodeString.charAt(ii);
				if( Code == '.')
				{
					Node = Node.GetLeft();				
				}
				else if (Code == '-')
				{
					Node = Node.GetRight();
				}
				else if( Code == ' ' || Node.IsLeaf() )
				{	
					Result.append(Node.GetItem());
					Node = _Root;
				}
				
				if(Node == null )
				{
					throw new NullPointerException("MorseTree::Decode can not decode " + ii + "th char of morse code: " + iCodeString );
				}
			}		
			return Result.toString() ;
		}
		
	}

