package com.litecom;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LCUIController {
	private Activity		_MainActivity;
	private Button			_ControlButton;
	private Button			_DecodeButton;
	private boolean			_CaptureOn = false;
	private boolean			_IsCameraSupportAELock = false;
	private LCSignalPlot	_SignalPlot;
	private Camera			_Camera;
	private FrameProcessor 	_FrameProcessor;
	private LCMorseSignalProcessor _MorseSignalProcessor;

		
	LCUIController(Activity iActivity, LCSignalPlot iSignalPlot, FrameProcessor iFrameProcessor, LCMorseSignalProcessor iMorseSignalProcessor)
	{		
		_MainActivity = iActivity;
		_ControlButton = (Button) _MainActivity.findViewById(R.id.ControlButton);
        //_ResultText = (TextView)findViewById(R.id.Result);
		_SignalPlot = iSignalPlot;
		_FrameProcessor  = iFrameProcessor;
		_MorseSignalProcessor = iMorseSignalProcessor;
		

		
		_ControlButton.setOnClickListener(new View.OnClickListener() {            
			public void onClick(View v) {
                // Perform action on click            	
            	_CaptureOn = !_CaptureOn;
            	
            	/*
            	if(_IsCameraSupportAELock && _CaptureOn)
            	{
            		Parameters CameraParam = _Camera.getParameters();
            		CameraParam.sksetAutoExposureLock(_CaptureOn);
            	}
            	else if(_IsCameraSupportAELock && !_CaptureOn)
            	{
            		Parameters CameraParam = _Camera.getParameters();
            		CameraParam.sksetAutoExposureLock(_CaptureOn);
            	}
            
            		*/
            	if(_CaptureOn)
            	{
            		_ControlButton.setText(R.string.Stop);
            		_MorseSignalProcessor.Reset();
            		_SignalPlot.Reset();
            	}
            	else
            	{
            		_ControlButton.setText(R.string.Start);
            		/*ArrayList<Integer> SignalSequence = _MorseSignalProcessor.GetBinarizedSignal();
    				for(int ii = 0 ; ii < SignalSequence.size(); ii ++)
    				{
    					_SignalPlot.PushData(SignalSequence.get(ii));
    				}*/
    				
    				try{
    					String Message = _MorseSignalProcessor.GetDecodedMessage();
    					new AlertDialog.Builder(_MainActivity).setTitle("Your message").setItems(new String[] { Message }, null).setNegativeButton("OK", null).show();
    				} catch ( Exception iE){
    					new AlertDialog.Builder(_MainActivity).setTitle(R.string.Error).setItems(new String[] {"Failed to decode message"}, null).setNegativeButton("OK", null).show();
    				}
    				
    				
            	}
            	
            	_FrameProcessor.SwitchCapture();
            	
            }
        });
	}
	
	public void SetCamera(Camera iCamera)
	{
		_Camera = iCamera;
		Parameters CameraParam = _Camera.getParameters();
		//_IsCameraSupportAELock = CameraParam.();
	}
	
}
